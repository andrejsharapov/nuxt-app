# Package.json

Update is not desirable at the moment

| Package                   | Version   |
| ------------------------- | --------- |
| "postcss":                | "^7.0.38" |
| "postcss-custom-media"    | "^7.0.8"  |
| "postcss-hexrgba"         | "^2.0.1"  |
| "postcss-html"            | "^1.3.0"  |
| "postcss-mixins"          | "^6.2.3"  |
| "postcss-nested"          | "^4.2.3"  |
| "postcss-responsive-type" | "^1.0.0"  |
| "postcss-sorting"         | "^5.0.1"  |
| "postcss-url"             | "^9.0.0"  |
| "postcss-utilities"       | "^0.8.4"  |
| "css-loader"              | "^5.0.0"  |
| "stylus"                  | "^0.54.8" |
| "stylus-loader"           | "4.3.1"   |
